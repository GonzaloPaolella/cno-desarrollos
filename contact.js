var form = document.querySelector('#form');

form.addEventListener('submit', function(event) {
    event.preventDefault();
    Swal.fire(
        '¡Envíado correctamente!',
        'Muchas gracias por confiar en nosotros, nos pondremos en contacto a la brevedad.',
        'success'
        )
        let formData = new FormData(form);
        form.reset();
        console.log(formData)
        fetch("contact.php",
        {
            body: formData,
            method: "post"
        }
    );
});

var form2 = document.querySelector('#form2');

form2.addEventListener('submit', function(event) {
    event.preventDefault();
    Swal.fire(
        '¡Envíado correctamente!',
        'Muchas gracias por confiar en nosotros, nos pondremos en contacto a la brevedad.',
        'success'
        )
        let formData = new FormData(form2);
        form2.reset();
        console.log(formData)
        fetch("contact.php",
        {
            body: formData,
            method: "post"
        }
    );
});