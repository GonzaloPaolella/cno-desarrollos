<?php 
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; 

if (isset($_GET['utm_source'])) {
    $utm_source = !($_GET['utm_source']) ? null : $_GET['utm_source'];
}

if (isset($_GET['utm_campaign'])) {
    $utm_campaign = !($_GET['utm_campaign']) ? null : $_GET['utm_campaign'];
}
if (isset($_GET['utm_medium'])) {
    $utm_medium = !($_GET['utm_medium']) ? null : $_GET['utm_medium'];
}

if (isset($_GET['utm_content'])) {
    $utm_content = !($_GET['utm_content']) ? null : $_GET['utm_content'];
}
if (isset($_GET['utm_term'])) {
    $utm_term = !($_GET['utm_term']) ? null : $_GET['utm_term'];
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CNO Desarrollos</title>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TMS5FHD');</script>
    <!-- End Google Tag Manager -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles.css">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <link href="sweet-alert.css" rel="stylesheet"/>

    <script type="text/javascript">
        function alert(){
            alert('¡Has envíado tus datos correctamente!');
        }
    </script>
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMS5FHD"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <header class="header-area">
        <div class="header-top">
            <div class="container">
                <div class="row header-wrap">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="logo">
                            <a href="/#">
                                <img src="images/logo-cno.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-6 d-none d-md-block">
                        <div class="company-info clearfix">
                            <div class="company-info-item">
                                <div class="icon">
                                    <img src="images/call.png" alt="">
                                </div>
                                <div class="info">
                                    <h6>(011) 5365-8398</h6>
                                    <p>de 08:00 a 17:30 hs</p>
                                </div>
                            </div>
                            <div class="company-info-item">
                                <div class="icon">
                                    <img src="images/email.png" alt="">
                                </div>
                                <div class="info">
                                    <h6>info@cnodesarrollos.com.ar</h6> 
                                    <p>Escribinos tu consulta.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sticky-header" class="header-middle-area transparent-header hidden-xs">
            <div class="container">
                <div class="drop-menu">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sticky-logo">
                                <a href="index-php">
                                    <img src="images/logo-cno.png" alt="">
                                </a>
                            </div>
                            <nav id="primary-menu">
                                <ul class="main-menu d-none d-md-block">
                                    <li>
                                        <a href="#">home</a>
                                    </li>
                                    <li>
                                        <a href="#amenities">ambientes</a>
                                    </li>
                                    <li>
                                        <a href="#3d">características</a>
                                    </li>
                                    <li class="m-0">
                                        <a href="#">contacto</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="mobile-menu-area d-md-none">
        <div class="container mean-container" style="display: flex; justify-content: center">
            <div class="mean-bar">
                <a href="#nav" class="meanmenu-reveal" data-toggle="collapse" data-target="nav" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <nav class="mean-nav collapse" id="nav">
                    <ul>
                        <li><a href="#home">home</a></li>
                        <li><a href="#amenities">ambientes</a></li>
                        <li><a href="#3d">características</a></li>
                        <li><a href="#contacto">contacto</a></li>
                    </ul>
                </nav>

            </div>
        </div>
    </div>

    <div id="home" class="slider bg-3 bg-opacity-black-40">
       <div class="wow animated animeatedFadeInUp fadeInUp">
           <div class="find-home-box">
               <div class="section-title text-white">
                   <h3>
                       <b>CNO</b>
                       <span style="font-size: 14px">®</span>
                   </h3>
                    <h3>TU MEJOR LUGAR</h3>
                    <h2>PARA INVERTIR</h2>
                    <h5>Dejá tus datos y un asesor te contactará</h5>
               </div>
               <div class="find-home">
                <form id="form" action="contact.php" method="POST">
                    <div class="row">
                        <div class="col-12">
                            <div class="find-home-item">
                                <input type="text" name="name" placeholder="Nombre y Apellido" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="find-home-item">
                                <input type="email" name="email"  placeholder="Email" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="find-home-item">
                                <input type="text"  name="phone" placeholder="Teléfono" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-5 col-xs-12">
                                    <div class="find-home-item">
                                        <input type="submit" class="button btn-block btn-hover-1"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Hidden UTM Fields -->
                    <input type="hidden" name="utm_source" value="<?php echo $utm_source; ?>">
                    <input type="hidden" name="utm_medium" value="<?php echo $utm_medium; ?>">
                    <input type="hidden" name="utm_campaign" value="<?php echo $utm_campaign; ?>">
                    <input type="hidden" name="utm_content" value="<?php echo $utm_content; ?>">
                    <input type="hidden" name="utm_term" value="<?php echo $utm_term; ?>">
                </form>

               </div>
           </div>
       </div> 
    </div>

    <section id="page-content">
        <div class="about-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="about-title">
                            <h3>Una inversión</h3>
                            <h2>para disfrutar la vida</h2>
                        </div>
                        <div class="about-info">
                            <p>
                                <span class="h6">La Zona:</span>
                                <br>
                                Una de las ultimas manzanas que combinan la cercanía, el confort  y calidad de vida  que Palermo puede ofrecer. Ubicada en el corazón de una de las áreas de mayor crecimiento gastronómico, habitacional y recreativo. Bares, restaurantes, ferias y locales comerciales que cuentan un nueva historia de un barrio que se redefine de manera constante.
                                Una de las últimas inversiones a la medida de lo que el público necesita hoy: modernidad, calidez, entretenimiento y confort.  Con una amplia propuesta de productos creados a la medida del inversor de hoy.   
                            </p>
                            <div class="author-quote">
                                <h3 class="mb-3">Una industria que se reactiva</h3>
                                <p><i class="far fa-check-circle"></i>El precio de las propiedades sigue escalando, una economía que se reactiva y oportunidades inmobiliarias que son cada vez mas escasas, todo resuelto en una propuesta única.</p>
                                <p><i class="far fa-check-circle"></i> Encontrá tu oportunidad con alguno de nuestros representantes.<br><strong>Escribinos a: info@cnodesarrollos.com.ar</strong><br><strong>o llamanos al: (11) 5365-8398 </strong></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="about-image">
                            <a href="images/sassari.jpg" target="_blank">
                                <img src="images/sassari-small.jpg" alt="">
                            </a>
                            <small style="color: grey">SASSARI BUILDING</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="amenities" class="services-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h2>Un espacio para invertir, vivir y soñar</h2>
                            <p>No se pierda la gran oportunidad de invertir en un espacio único en un lugar único.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="service-carousel">
                        <div class="slick-list">
                            <div class="slick-track">
                                <div class="row">
                                    <div class="col-12 col-md-4 col-lg-4">
                                        <a href="images/amenities/big-hall.gif" target="_blank">
                                            <div class="service-item">
                                                <div class="service-image">
                                                    <img src="images/amenities/hall.png" alt="">
                                                </div>
                                                <div class="service-info">
                                                    <h5>Hall de entrada</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-4">
                                        <a href="images/amenities/big-living.jpeg" target="_blank">
                                            <div class="service-item">
                                                <div class="service-image">
                                                    <img src="images/amenities/living.png" alt="">
                                                </div>
                                                <div class="service-info">
                                                    <h5>Living Comedor</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-4">
                                        <a href="images/amenities/big-dormitorios.jpeg" target="_blank">
                                            <div class="service-item">
                                                <div class="service-image">
                                                    <img src="images/amenities/dormitorios.png" alt="">
                                                </div>
                                                <div class="service-info">
                                                    <h5>Dormitorio</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-4">
                                        <a href="images/amenities/big-cocina.jpeg" target="_blank">
                                            <div class="service-item">
                                                <div class="service-image">
                                                    <img src="images/amenities/cocina.png" alt="">
                                                </div>
                                                <div class="service-info">
                                                    <h5>Cocina</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-4">
                                        <a href="images/amenities/big-terraza.jpeg" target="_blank">
                                            <div class="service-item">
                                                <div class="service-image">
                                                    <img src="images/amenities/terraza.png" alt="">
                                                </div>
                                                <div class="service-info">
                                                    <h5>Patio Interno</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-4">
                                        <a href="images/amenities/big-baño.jpeg" target="_blank">
                                            <div class="service-item">
                                                <div class="service-image">
                                                    <img src="images/amenities/baño.png" alt="">
                                                </div>
                                                <div class="service-info">
                                                    <h5>Baño</h5>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- <div class="col-12 col-md-4 col-lg-4">
                                        <div class="service-item">
                                            <div class="service-image">
                                                <img src="http://avalon3.com/a-invest/images/service/7.jpg" alt="">
                                            </div>
                                            <div class="service-info">
                                                <h5>Sum</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-3">
                                        <div class="service-item">
                                            <div class="service-image">
                                                <img src="http://avalon3.com/a-invest/images/service/8.jpg" alt="">
                                            </div>
                                            <div class="service-info">
                                                <h5>Exteriores</h5>
                                            </div>
                                        </div>
                                    </div>       -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="3d" class="features-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md 7 offset-md-5">
                            <div class="features-info">
                                <div class="section-title">
                                    <h3>CARACTERÍSTICAS ÚNICAS</h3>
                                    <h2 class="h1">DE ESTOS EMPRENDIMIENTOS</h2>
                                </div>
                                <div class="feauteres-desc">
                                    <p>
                                        <span style="font-weight: bold">COMPLEJO AREZZO® y COMPLEJO SASSARI®</span>
                                        lo va a sorprender
                                    </p>
                                </div>
                                <div class="features-include">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <div class="features-include-list">
                                                <h6>
                                                    <img src="http://avalon3.com/a-invest/images/icons/7.png" alt="">
                                                    EL COMPLEJO:
                                                </h6>
                                                <p>- Excelente acceso <br>- Hall de entrada con importante iluminación</p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="features-include-list">
                                                <h6>
                                                    <img src="http://avalon3.com/a-invest/images/icons/7.png" alt="">
                                                    INSTALACIONES DE COMFORT:
                                                </h6>
                                                <p>- Conexión de TV por cable y teléfono <br>- Portero eléctrico <br>- Aire acondicionado frío / calor <br>- Puertas Pentágono</p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="features-include-list">
                                                <h6>
                                                    <img src="http://avalon3.com/a-invest/images/icons/7.png" alt="">
                                                    AMENITIES:
                                                </h6>
                                                <p>- Piscina <br>- Parrilla <br>- Solarium<br>- Public Laundry <br></p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="features-include-list">
                                                <h6>
                                                    <img src="http://avalon3.com/a-invest/images/icons/7.png" alt="">
                                                    FINANCIAMIENTO:
                                                </h6>
                                                <p>- 30% al boleto de Compraventa, y 12 cuotas fijas consecutivas</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="featured-flat-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title">
                                <h2>
                                    PLANOS COMPLEJO AREZZO
                                    <span style="font-size: 14px;">®</span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="featured-flat">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="properties">
                                    <a href="images/arezzo-1.png" target="_blank">
                                        <img src="images/arezzo-1.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="properties">
                                    <a href="images/arezzo-2.png" target="_blank">
                                        <img src="images/arezzo-2.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="properties">
                                    <a href="images/arezzo-3.png" target="_blank">
                                        <img src="images/arezzo-3.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 100px;">
                        <div class="col-md-12">
                            <div class="section-title">
                                <h2>
                                    PLANOS COMPLEJO SASSARI
                                    <span style="font-size: 14px;">®</span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="featured-flat" style="padding-top: 25px;">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="properties">
                                    <a href="images/sassari-1.png" target=_blank>
                                        <img src="images/sassari-1.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="properties">
                                    <a href="images/sassari-2.png" target="_blank">
                                        <img src="images/sassari-2.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer-area bg-2 bg-opacity-black-90">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget">
                                <h6 class="footer-title">CONTACTANOS</h6>
                                <ul class="footer-address">
                                    <li>
                                        <div class="address-icon">
                                            <img src="http://avalon3.com/a-invest/images/icons/location-2.png" alt="">
                                        </div>
                                        <div class="address-info">
                                            <span>Palermo</span>
                                            <span>Buenos Aires, Argentina</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="address-icon">
                                            <img src="http://avalon3.com/a-invest/images/icons/phone-3.png" alt="">
                                        </div>
                                        <div class="address-info">
                                            <span>Teléfono: (011) 5365-8398</span>
                                            <span>Celular: (+54) 11 5249-1991</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="address-icon">
                                            <img src="http://avalon3.com/a-invest/images/icons/world.png" alt="">
                                        </div>
                                        <div class="address-info">
                                            <span>Email: info@cnodesarrollos.com.ar</span>
                                            <span>Web: <a href="http://cnodesarrollos.com.ar/" target="_blank">www.cnodesarrollos.com.ar</a></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="col-lg-6 col-md-5 d-none-sm col-xs-12">
                            <div class="footer-widget middle">
                                <h6 class="footer-title">Otras Oportunidades</h6>
                                <ul class="footer-lastest-news">
                                    <li>
                                        <div class="lastest-news-image">
                                            <a href="#">
                                                <img src="" alt="IMG">
                                            </a>
                                        </div>
                                        <div class="lastest-news-info">
                                            <h6>PRÓXIMAMENTE</h6>
                                            <P>Las mejores propiedades abiertas.</P>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="lastest-news-image">
                                            <a href="#">
                                                <img src="" alt="IMG">
                                            </a>
                                        </div>
                                        <div class="lastest-news-info">
                                            <h6>PRÓXIMAMENTE</h6>
                                            <P>Las mejores propiedades abiertas.</P>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="lastest-news-image">
                                            <a href="#">
                                                <img src="" alt="IMG">
                                            </a>
                                        </div>
                                        <div class="lastest-news-info">
                                            <h6>PRÓXIMAMENTE</h6>
                                            <P>Las mejores propiedades abiertas.</P>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget">
                                <h6 class="footer-title">DÓNDE ESTAMOS</h6>
                                <div class="footer-contact">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3284.250953205627!2d-58.41370478477054!3d-34.59781518046114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcca8f331de731%3A0x6fcd9a56b969c4d9!2sAv.%20C%C3%B3rdoba%203179%2C%20C1187AAF%20CABA!5e0!3m2!1ses-419!2sar!4v1580841541556!5m2!1ses-419!2sar" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="copyright">
                                <p>
                                    Copyright © 2020
                                    <a href="http://cnodesarrollos.com.ar/"><strong> CNO Desarrollos</strong></a>
                                    . All rights reserved.
                                </p>
                                <p>By <a href="https://www.wemanagement.com.ar">We Management</a></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </footer>
    </section>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/7aa97e8cf7.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script src="contact.js"></script>
    
    <script>
        function alert(){
            Swal.fire('¡Envíado correctamente!',
            'Muchas gracias por confiar en nosotros, nos pondremos en contacto a la brevedad.',
            'success'
            );
        }
    </script>

    <script>
        window.onscroll = function() {myFunction()};

        var header = document.getElementById("sticky-header");
        var sticky = header.offsetTop;

        function myFunction() {
            if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
            } else {
                header.classList.remove("sticky");
            }
        }
    </script>
       <!-- Linkando JS-->
        <script type="text/javascript" src="https://matheusfreitas.com/mercatorio/js/jquery.min.js"></script>
        <script type="text/javascript" src="https://matheusfreitas.com/mercatorio/js/floating-wpp.js"></script>
    <div class="floating-wpp"></div><!--Left-->
    <script>
    

        // Rightt Propriedades
        $(function () {
                $('.Right-zap').floatingWhatsApp({
                headerTitle: 'Whats App!',
                phone: '541140471058',
                popupMessage: 'Hola, en que te puedo ayudar?',
                message: "Hola, quiero más información",
                showPopup: true,
                position: 'right',
                showOnIE: true,
                zindex: 40,
                size: '60px',
                // buttonImage: '<img src="https://matheusfreitas.com/mercatorio/img/whatsapp.svg"/>'
                });
            });



    </script>

</body>
</html>
{{-- 
<head>
    <meta charset="UTF-8">
    <title>Floating Whats App Button</title>
  <!-- Linkando CSS-->
 </head>
  
 <body>
 <!--Github Documentation on the link https://github.com/rafaelbotazini/floating-whatsapp -->
   <div class="Right-zap"></div><!--Right-->
   
 <!-- === Minha abinha do Siga no GitHub === -->
     <a class="fork" target="_blank" href="https://github.com/matheusfreitas70"><img style="position: absolute; top: 0; right: 0; border: 0;" src="https://www.destinoisrael.com.br/imgs/imge-sigame-github.png" title="Me siga no GitHub" alt="Me siga no GitHub"></a>
   
   <!-- Linkando JS-->
   <script type="text/javascript" src="https://matheusfreitas.com/mercatorio/js/jquery.min.js"></script>
   <script type="text/javascript" src="https://matheusfreitas.com/mercatorio/js/floating-wpp.js"></script>
 </body>
  --}}